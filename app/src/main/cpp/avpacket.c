#include "avpacket.h"
#include <jni.h>
#include <assert.h>
#include "jniutil.h"

static jfieldID fid_packet_handle;

void packet_delete(struct packet *packet) {
    av_packet_free(&packet->packet);
    free(packet);
}

struct packet *packet_get_handle(JNIEnv *env, jobject java_packet) {
    return (struct packet *) (*env)->GetLongField(env, java_packet, fid_packet_handle);
}

void packet_set_handle(JNIEnv *env, jobject java_packet, struct packet *packet) {
    (*env)->SetLongField(env, java_packet, fid_packet_handle, (intptr_t) packet);
}

JNIEXPORT void JNICALL
Java_com_rom1v_mycamerasample_AVPacket_initIDs(JNIEnv *env, jclass clazz) {
    fid_packet_handle = (*env)->GetFieldID(env, clazz, "handle", "J");
}

JNIEXPORT jlong JNICALL
Java_com_rom1v_mycamerasample_AVPacket_create(JNIEnv *env, jobject thiz, jobject codec_buffer, jint position,
                                              jint remaining, jboolean is_config, jlong pts) {
    struct packet *packet = malloc(sizeof(*packet));
    if (!packet) {
        jni_throw_oom(env);
        return 0;
    }

    packet->packet = av_packet_alloc();
    if (!packet->packet) {
        free(packet);
        jni_throw_oom(env);
        return 0;
    }

    uint8_t *buf = (*env)->GetDirectBufferAddress(env, codec_buffer);
    assert(buf);

    uint8_t *data = av_malloc(remaining);
    if (!data) {
        av_packet_free(&packet->packet);
        free(packet);
        jni_throw_oom(env);
        return 0;
    }

    memcpy(data, buf + position, remaining);

    int ret = av_packet_from_data(packet->packet, data, remaining);
    if (ret) {
        free(data);
        av_packet_free(&packet->packet);
        free(packet);
        jni_throw_oom(env);
        return 0;
    }

    packet->packet->pts = is_config ? AV_NOPTS_VALUE : pts;
    packet->packet->dts = packet->packet->pts;

    packet->is_config = is_config;

    return (intptr_t) packet;
}

JNIEXPORT void JNICALL
Java_com_rom1v_mycamerasample_AVPacket_release(JNIEnv *env, jobject thiz) {
    struct packet *packet = packet_get_handle(env, thiz);
    assert(packet);
    packet_set_handle(env, thiz, NULL);
    packet_delete(packet);
}
