#ifndef MYCAMERASAMPLE_JNIUTIL_H
#define MYCAMERASAMPLE_JNIUTIL_H

void jni_throw_by_name(JNIEnv *env, const char *name, const char *msg);

static inline jni_throw_oom(JNIEnv *env) {
    jni_throw_by_name(env, "java/lang/OutOfMemoryError", NULL);
}

static inline jni_throw_ffmpeg(JNIEnv *env, const char *msg) {
    jni_throw_by_name(env, "com/rom1v/mycamerasample/FFmpegException", msg);
}

#endif //MYCAMERASAMPLE_JNIUTIL_H
