// Write C++ code here.
//
// Do not forget to dynamically load the C++ library into your application.
//
// For instance,
//
// In MainActivity.java:
//    static {
//       System.loadLibrary("mycamerasample");
//    }
//
// Or, in MainActivity.kt:
//    companion object {
//      init {
//         System.loadLibrary("mycamerasample")
//      }
//    }

#include <jni.h>
#include <assert.h>
#include <android/log.h>
#include <stdbool.h>
#include <string.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>

#include "avpacket.h"
#include "jniutil.h"

struct sys {
    AVFormatContext *avf_ctx;
    bool header_written;
    AVPacket *pending;
};

static const AVRational ANDROID_TIME_BASE = {1, 1000000}; // timestamps in us

static jfieldID fid_sys;

static inline struct sys *GetSys(JNIEnv *env, jobject ffmpegStreamer) {
    return (struct sys *) (*env)->GetLongField(env, ffmpegStreamer, fid_sys);
}

JNIEXPORT void JNICALL
Java_com_rom1v_mycamerasample_FFmpegStreamer_initIDs(JNIEnv *env, jclass clazz) {
    fid_sys = (*env)->GetFieldID(env, clazz, "sys", "J");
}

JNIEXPORT jboolean JNICALL
Java_com_rom1v_mycamerasample_FFmpegStreamer_init(
        JNIEnv* env,
        jobject obj,
        int width,
        int height,
        jstring dest) {
    struct sys *sys = malloc(sizeof(*sys));
    if (!sys) {
        jni_throw_oom(env);
        return JNI_FALSE;
    }

    sys->header_written = false;
    sys->pending = NULL;

    const jbyte *out_url = (*env)->GetStringUTFChars(env, dest, NULL);
    if (!out_url) {
        free(sys);
        return JNI_FALSE; /* OutOfMemoryError already thrown */
    }

    int ret = avformat_alloc_output_context2(&sys->avf_ctx, NULL, "rtp_mpegts", out_url);
    if (ret < 0) {
        (*env)->ReleaseStringUTFChars(env, dest, out_url);
        free(sys);
        jni_throw_oom(env);
        return JNI_FALSE;
    }

    const AVCodec *codec = avcodec_find_decoder(AV_CODEC_ID_H264);
    assert(codec);
    AVStream *ostream = avformat_new_stream(sys->avf_ctx, codec);
    if (!ostream) {
        (*env)->ReleaseStringUTFChars(env, dest, out_url);
        free(sys);
        return JNI_FALSE;
    }

    ostream->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
    ostream->codecpar->codec_id = codec->id;
    //ostream->codecpar->format = AV_PIX_FMT_YUV420P;
    ostream->codecpar->width = width;
    ostream->codecpar->height = height;

    ret = avio_open(&sys->avf_ctx->pb, out_url, AVIO_FLAG_WRITE);
    (*env)->ReleaseStringUTFChars(env, dest, out_url);
    if (ret) {
        avformat_free_context(sys->avf_ctx);
        free(sys);
        return JNI_FALSE;
    }

    __android_log_print(ANDROID_LOG_INFO, "MyCameraSample", "global_header? %d", sys->avf_ctx->oformat->flags & AVFMT_GLOBALHEADER);
    (*env)->SetLongField(env, obj, fid_sys, (intptr_t) sys);
    return JNI_TRUE;
}

JNIEXPORT void JNICALL
Java_com_rom1v_mycamerasample_FFmpegStreamer_releaseNative(JNIEnv *env, jobject thiz) {
    struct sys *sys = GetSys(env, thiz);
    assert(sys);

    avio_close(sys->avf_ctx->pb);
    avformat_free_context(sys->avf_ctx);
}

static bool ffmpegstreamer_write_header(JNIEnv *env, struct sys *sys, const AVPacket *packet) {
    AVStream *ostream = sys->avf_ctx->streams[0];

    uint8_t *extradata = av_malloc(packet->size * sizeof(uint8_t));
    if (!extradata) {
        jni_throw_oom(env);
        return false;
    }

    memcpy(extradata, packet->data, packet->size);

    ostream->codecpar->extradata = extradata;
    ostream->codecpar->extradata_size = packet->size;

    int ret = avformat_write_header(sys->avf_ctx, NULL);
    if (ret < 0) {
        jni_throw_ffmpeg(env, "Could not write RTP header");
        return false;
    }

    __android_log_print(ANDROID_LOG_INFO, "MyCameraSample", "==== write header ok");

    return true;
}

static void ffmpegstreamer_rescale_packet(struct sys *sys, const AVPacket *packet) {
    AVStream *ostream = sys->avf_ctx->streams[0];
    av_packet_rescale_ts(packet, ANDROID_TIME_BASE, ostream->time_base);
}

static bool ffmpegstreamer_write(JNIEnv *env, struct sys *sys, struct packet *packet) {
    if (!sys->header_written) {
        if (!packet->is_config) {
            jni_throw_ffmpeg(env, "The first packet is not a config packet");
            return false;
        }

        bool ok = ffmpegstreamer_write_header(env, sys, packet->packet);
        if (!ok) {
            /* Exception already thrown */
            return false;
        }

        sys->header_written = true;
        return true;
    }

    if (packet->is_config) {
        // ignore config packets
        return true;
    }

    ffmpegstreamer_rescale_packet(sys, packet->packet);
    int ret = av_write_frame(sys->avf_ctx, packet->packet);

    if (ret < 0) {
        jni_throw_ffmpeg(env, "Could not write frame");
        return false;
    }

    return true;
}

static bool ffmpegstreamer_send(JNIEnv *env, struct sys *sys, struct packet *packet) {
    // A config packet must not be demuxed immediately (it contains no frame); instead, it must be concatenated with
    // the future data packet.
    if (sys->pending || packet->is_config) {
        if (sys->pending) {
            size_t offset = sys->pending->size;
            if (av_grow_packet(sys->pending, packet->packet->size)) {
                jni_throw_oom(env);
                return false;
            }

            memcpy(sys->pending->data + offset, packet->packet->data, packet->packet->size);
        } else {
            sys->pending = av_packet_alloc();
            if (!sys->pending) {
                jni_throw_oom(env);
                return false;
            }
            if (av_packet_ref(sys->pending, packet->packet)) {
                av_packet_free(&sys->pending);
                jni_throw_oom(env);
                return false;
            }
        }

        if (!packet->is_config) {
            // prepare the concat packet
            sys->pending->pts = packet->packet->pts;
            sys->pending->dts = packet->packet->dts;
            sys->pending->flags = packet->packet->flags;

            // replace the current packet by the concat one
            av_packet_free(&packet->packet);
            packet->packet = sys->pending;
            sys->pending = NULL;
        }
    }

    return ffmpegstreamer_write(env, sys, packet);
}

static void ffmpegstreamer_log_packet(const AVPacket *packet) {
    int size = packet->size;
    if (size > 48)
        size = 48;
    char *tmp = malloc(size * 3 + 1);

    for (int i = 0; i < size; ++i)
        snprintf(tmp + i * 3, 4, "%02X ", packet->data[i]);
    tmp[size * 3] = '\0';
    __android_log_print(ANDROID_LOG_INFO, "MyCameraSample", "==== packet (pts=%ld, size=%d): %s%s",
                        (long) packet->pts, packet->size, tmp, packet->size > 48 ? "..." : "");
}

JNIEXPORT void JNICALL
Java_com_rom1v_mycamerasample_FFmpegStreamer_send(JNIEnv *env, jobject thiz, jobject java_packet) {
    struct sys *sys = GetSys(env, thiz);
    assert(sys);

    struct packet *packet = packet_get_handle(env, java_packet);
    assert(packet);

    // Take ownership of the packet, so reset the caller handle
    packet_set_handle(env, java_packet, NULL);

    ffmpegstreamer_log_packet(packet->packet);

    bool ok = ffmpegstreamer_send(env, sys, packet);
    packet_delete(packet);
    if (!ok) {
        /* Exception already thrown */
        return;
    }
}
