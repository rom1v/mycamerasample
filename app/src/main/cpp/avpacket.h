#ifndef MYCAMERASAMPLE_AVPACKET_H
#define MYCAMERASAMPLE_AVPACKET_H

#include <jni.h>
#include <libavformat/avformat.h>

struct packet {
    AVPacket *packet;
    jboolean is_config;
};

void packet_delete(struct packet *packet);

struct packet *packet_get_handle(JNIEnv *env, jobject java_packet);
void packet_set_handle(JNIEnv *env, jobject java_packet, struct packet *packet);

#endif //MYCAMERASAMPLE_AVPACKET_H
