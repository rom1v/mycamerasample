package com.rom1v.mycamerasample;

import android.media.MediaCodec;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class RtpSocket implements Runnable {

    private static final String TAG = MainActivity.TAG;

    public static final int RTP_HEADER_LENGTH = 12;
    public static final int MTU = 1300;

    private final InetAddress destIp;
    private final int destPort;

    private final Thread thread;
    private final BlockingQueue<NALPacket> queue = new ArrayBlockingQueue(500);

    private short seq;

    public RtpSocket(InetAddress destIp, int destPort) {
        this.destIp = destIp;
        this.destPort = destPort;

        thread = new Thread(this);
        thread.start();
    }

    public int createSsrc() {
        return new Random().nextInt();
    }

    public void sendH264Packet(ByteBuffer codecBuffer, MediaCodec.BufferInfo bufferInfo, int ssrc) {
        boolean isConfig = (bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0;
        long pts = bufferInfo.presentationTimeUs;

        int prefix = codecBuffer.getInt(); // consume NAL prefix 0x00000001

        // TODO write directly into the target buffer to avoid an intermediate copy
        byte[] tmp = new byte[codecBuffer.remaining()];
        codecBuffer.get(tmp);

        try {
            if (isConfig) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < tmp.length; ++i) {
                    builder.append(String.format("%02X ", tmp[i]));
                }
                Log.i(TAG, "config: " + builder);

                // there might be several NALs
                int offset = 0;
                int prefixIndex;
                do {
                    Log.i(TAG, "ff");
                    prefixIndex = NALPacket.findPrefix(tmp, offset);
                    Log.i(TAG, "prefixIndex = " + prefixIndex);
                    NALPacket packet;
                    int end = prefixIndex != -1 ? prefixIndex : tmp.length;
                    Log.i(TAG, "offset = " + offset + "; " + (end - offset));
                    packet = NALPacket.createFrom(ssrc, pts, tmp, offset, end - offset);
                    builder = new StringBuilder();
                    for (int i = packet.getOffset(); i < packet.getOffset() + packet.getLength(); ++i) {
                        builder.append(String.format("%02X ", packet.getData()[i]));
                    }
                    Log.i(TAG, "packet: " + builder);

                    queue.put(packet);
                    if (prefixIndex != -1) {
                        offset = prefixIndex + 4;
                    }
                } while (prefixIndex != -1);
            } else {
                NALPacket packet = NALPacket.createFrom(ssrc, pts, tmp, 0, tmp.length);
                queue.put(packet);
            }
        } catch (InterruptedException e) {
            // ignore
        }
    }

    public void stop() {
        thread.interrupt();
    }

    public void run() {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
            DatagramPacket datagram = new DatagramPacket(new byte[0], 0, destIp, destPort);
            while (!Thread.currentThread().isInterrupted()) {
                NALPacket packet = queue.take();

                byte[] data = packet.getData();
                int offset = packet.getOffset();
                int length = packet.getLength();
                int ts = packet.getTs();
                int ssrc = packet.getSsrc();
                assert (offset >= 12); // we need space in front to avoid allocations

                if (length <= MTU - RTP_HEADER_LENGTH) {
                    Log.i(TAG, "Single NAL unit ==== ");
                    // Single NAL unit <https://datatracker.ietf.org/doc/html/rfc6184#section-5.6>
                    writeRtpHeaders(data, offset - 12, true, seq++, ts, ssrc);
                    datagram.setData(data, offset - 12, length + 12);
                    socket.send(datagram);
                } else {
                    // FU-A <https://datatracker.ietf.org/doc/html/rfc6184#section-5.8>
                    byte fNri = (byte) (data[offset] & 0x60);
                    byte nalType = (byte) (data[offset] & 0x1F);
                    int fragOffset = offset + 1; // ignore the NAL header, it's "included" in the 2-byte FU headers
                    boolean start = true;
                    int remaining = length - 1;
                    while (remaining > 0) {
                        boolean end = remaining <= MTU - RTP_HEADER_LENGTH - 2;
                        writeRtpHeaders(data, fragOffset - 14, end, seq++, ts, ssrc);
                        writeFuAHeaders(data, fragOffset - 2, fNri, start, end, nalType);

                        int packetLength = end ? remaining : MTU - RTP_HEADER_LENGTH - 2;

                        datagram.setData(data, fragOffset - 14, packetLength + 14);
//                        StringBuilder builder = new StringBuilder();
//                        for (int i = 0; i < 14; ++i) {
//                            builder.append(String.format("%02X ", data[fragOffset - 14 + i]));
//                        }
//                        Log.i(TAG, "FU-A ==== " + builder + " [" + start + "|" + end + "]");
                        remaining -= packetLength;
                        fragOffset += packetLength;
                        start = false;
                        socket.send(datagram);
                    }
                }
            }
        } catch (InterruptedException e) {
            // end of thread
        } catch (IOException e) {
            Log.e(TAG, "Could not send packet", e);
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }


    private static void writeRtpHeaders(byte[] data, int offset, boolean marker, short seq, int ts, int ssrc) {
        data[offset] = (byte) 0x80;
        data[offset + 1] = (byte) 96;
        if (marker) {
            data[offset + 1] |= 0x80;
        }
        data[offset + 2] = (byte) (seq >> 8);
        data[offset + 3] = (byte) seq;
        data[offset + 4] = (byte) (ts >> 24);
        data[offset + 5] = (byte) (ts >> 16);
        data[offset + 6] = (byte) (ts >> 8);
        data[offset + 7] = (byte) ts;
        data[offset + 8] = (byte) (ssrc >> 24);
        data[offset + 9] = (byte) (ssrc >> 16);
        data[offset + 10] = (byte) (ssrc >> 8);
        data[offset + 11] = (byte) ssrc;
    }

    private static void writeFuAHeaders(byte[] data, int offset, byte fNri, boolean start, boolean end, byte nalType) {
        data[offset] = (byte) (fNri | 28); // 28: FU-A
        data[offset + 1] = nalType;
        if (start) {
            data[offset + 1] |= 0x80;
        } else if (end) {
            data[offset + 1] |= 0x40;
        }
    }
}
