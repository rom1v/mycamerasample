package com.rom1v.mycamerasample;

public class FFmpegException extends Exception {
    public FFmpegException(String message) {
        super(message);
    }
}
