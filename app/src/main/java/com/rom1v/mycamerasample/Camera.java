package com.rom1v.mycamerasample;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.util.Log;
import android.view.Surface;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Camera {

    interface Callback {
        void onStarted();

        void onFailed();
    }

    private static final String TAG = MainActivity.TAG;
    private static final Executor executor = Executors.newSingleThreadExecutor();

    private CameraDevice camera;
    private CameraCharacteristics characteristics;

    private SharedSurface surface;
    private CameraCaptureSession session; // protected by "this" lock

    public Camera(CameraDevice camera, CameraCharacteristics characteristics) {
        this.camera = camera;
        this.characteristics = characteristics;
    }

    public void start(final SharedSurface surface, final Callback listener) {
        this.surface = surface;
        surface.ref();

        OutputConfiguration outputConfig = new OutputConfiguration(surface.get());
        List<OutputConfiguration> outputs = Arrays.asList(outputConfig);
        SessionConfiguration sessionConfig = new SessionConfiguration(SessionConfiguration.SESSION_REGULAR, outputs, executor,
                new CameraCaptureSession.StateCallback() {
                    @Override
                    public void onConfigured(@NonNull CameraCaptureSession session) {
                        CaptureRequest req = createCaptureRequest(session.getDevice(), surface.get());
                        try {
                            session.setRepeatingRequest(req, null, null);
                        } catch (CameraAccessException e) {
                            Log.e(TAG, "Could not set repeating request", e);
                            listener.onFailed();
                        }

                        synchronized (Camera.this) {
                            Camera.this.session = session;
                        }
                        listener.onStarted();
                    }

                    @Override
                    public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                        Log.w(TAG, "Camera session configure failed");
                        listener.onFailed();
                    }
                });

        try {
            camera.createCaptureSession(sessionConfig);
        } catch (CameraAccessException e) {
            Log.e(TAG, "Could not create capture session", e);
            listener.onFailed();
        }
    }

    private static CaptureRequest createCaptureRequest(CameraDevice device, Surface surface) {
        try {
            CaptureRequest.Builder builder = device.createCaptureRequest(CameraDevice.TEMPLATE_ZERO_SHUTTER_LAG);
            builder.addTarget(surface);
            return builder.build();
        } catch (CameraAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        synchronized(this) {
            if (session != null) {
                try {
                    session.stopRepeating();
                    session.abortCaptures();
                } catch (CameraAccessException e) {
                    Log.e(TAG, "Could not stop repeating", e);
                }
                session.close();
                session = null;
            }
        }
        if (camera != null) {
            camera.close();
        }
    }

    public void release() {
        if (surface != null && surface.unref()) {
            surface = null;
        }
    }
}
