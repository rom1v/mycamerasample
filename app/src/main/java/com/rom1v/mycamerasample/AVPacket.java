package com.rom1v.mycamerasample;

import android.media.MediaCodec;

import java.nio.ByteBuffer;

public class AVPacket {

    static {
        initIDs();
    }
    private static native void initIDs();

    private long handle;

    AVPacket(ByteBuffer codecBuffer, MediaCodec.BufferInfo bufferInfo) {
        int position = codecBuffer.position();
        int remaining = codecBuffer.remaining();
        boolean isConfig = (bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0;
        long pts = bufferInfo.presentationTimeUs;
        handle = create(codecBuffer, position, remaining, isConfig, pts);
    }

    private native long create(ByteBuffer codecBuffer, int position, int remaining, boolean isConfig, long pts);

    public native void release();
}
