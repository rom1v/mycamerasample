package com.rom1v.mycamerasample;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaCodec;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class MainActivity extends Activity {

    public static final String TAG = "MyCameraSample";

    private static final int REQUEST_CODE_PERMISSION_CAMERA = 1;

    private Camera camera;
    private Encoder encoder;

    private Button button;
    private boolean started;

    static class CameraSelection {
        String id;
        CameraCharacteristics characteristics;

        CameraSelection(String id, CameraCharacteristics characteristics) {
            this.id = id;
            this.characteristics = characteristics;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!started) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        button.setEnabled(false);
                        open();
                    } else {
                        String[] permissions = {Manifest.permission.CAMERA};
                        requestPermissions(permissions, REQUEST_CODE_PERMISSION_CAMERA);
                    }
                    started = true;
                } else {
                    if (encoder != null) {
                        encoder.stop();
                        encoder.release();
                        encoder = null;
                    }
                    if (camera != null) {
                        camera.stop();
                        camera.release();
                        camera = null;
                    }
                    button.setText(R.string.start);
                    started = false;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (encoder != null) {
            encoder.stop();
            encoder.release();
        }
        if (camera != null) {
            camera.stop();
            camera.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                open();
            }
        }
    }

    private void open() {
        // This method should be called only once the permission is granted, but Android is not happy if we don't check explicitly
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            throw new AssertionError();
        }

        final CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        CameraSelection selection = getBackCameraSelection(cameraManager);
        String cameraId = selection.id;
        final CameraCharacteristics cameraCharacteristics = selection.characteristics;
        try {
            cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice camera) {
                    startSession(cameraManager, cameraCharacteristics, camera);
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice camera) {
                    Log.w(TAG, "Camera has been disconnected");
                    finish();
                }

                @Override
                public void onError(@NonNull CameraDevice camera, int error) {
                    Log.e(TAG, "Camera error: " + error);
                    finish();
                }
            }, new Handler(Looper.myLooper()));
        } catch (CameraAccessException e) {
            Log.e(TAG, "Could not open camera", e);
        }
        // TODO createCaptureSession
    }

    private void startSession(CameraManager cameraManager, CameraCharacteristics cameraCharacteristics, CameraDevice device) {
        // called from the UI thread
        StreamConfigurationMap configs = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        Size[] sizes = configs.getOutputSizes(MediaCodec.class);
        Size size = selectSize(sizes, 1920, 1080);
        if (size == null) {
            throw new RuntimeException("No supported size");
        }

        Log.i(TAG, "startSession " + size.getWidth() + " × " + size.getHeight());

        InetAddress destIp;
        try {
            destIp = Inet4Address.getByName("192.168.1.74");
        } catch (UnknownHostException e) {
            Log.e(TAG, "Unknown host", e);
            finish();
            return;
        }

        final FFmpegStreamer ffmpeg = new FFmpegStreamer(size.getWidth(), size.getHeight(), "rtp://192.168.1.74:1234");

        try {
            encoder = new Encoder(size.getWidth(), size.getHeight(), new Encoder.Callback() {
                @Override
                public void onPacket(ByteBuffer codecBuffer, MediaCodec.BufferInfo bufferInfo) {
                    AVPacket packet = new AVPacket(codecBuffer, bufferInfo);
                    ffmpeg.submit(packet);
                    //socket.sendH264Packet(codecBuffer, bufferInfo, ssrc);
                }
            });
        } catch (IOException e) {
            Log.e(TAG, "Could not create encoder", e);
            finish();
            return;
        }

        this.camera = new Camera(device, cameraCharacteristics);
        this.camera.start(encoder.getSurface(), new Camera.Callback() {
            @Override
            public void onStarted() {
                Log.i(TAG, "============ onStarted");
                encoder.start();
            }

            @Override
            public void onFailed() {
                Log.i(TAG, "============ onFailed");
                runOnUiThread(() -> {
                    finish();
                });
            }
        });

        button.setEnabled(true);
        button.setText(R.string.stop);
    }

    private Size selectSize(Size[] sizes, int targetLongest, int targetShortest) {
        Size result = null;
        int resultLongest = 0;
        int resultShortest = 0;
        for (Size size : sizes) {
            int longest = Math.max(size.getWidth(), size.getHeight());
            int shortest = Math.min(size.getWidth(), size.getHeight());
            if (longest <= targetLongest && longest > resultLongest || (longest == resultLongest && shortest <= targetShortest && shortest > resultShortest)) {
                result = size;
                resultLongest = longest;
                resultShortest = shortest;
            }
        }
        return result;
    }

    private CameraSelection getBackCameraSelection(CameraManager cameraManager) {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cc = cameraManager.getCameraCharacteristics(cameraId);
                int lensFacing = cc.get(CameraCharacteristics.LENS_FACING);
                if (lensFacing == CameraCharacteristics.LENS_FACING_BACK) {
                    return new CameraSelection(cameraId, cc);
                }
            }
        } catch (CameraAccessException e) {
            Log.e(TAG, "Cannot list camera", e);
        }
        return null;
    }
}
