package com.rom1v.mycamerasample;

import android.media.MediaCodec;
import android.util.Log;

import java.nio.ByteBuffer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class FFmpegStreamer implements Runnable {

    static {
        System.loadLibrary("mycamerasample");
        initIDs();
    }

    private static final String TAG = MainActivity.TAG;

    private final Thread thread;
    private final BlockingQueue<AVPacket> queue = new ArrayBlockingQueue<>(500);

    @SuppressWarnings("unused")
    private long sys; // pointer to private native structure

    public FFmpegStreamer(int width, int height, String dest) {
        if (!init(width, height, dest)) {
            throw new RuntimeException("Could not initialize FFmpegStreamer");
        }

        thread = new Thread(this);
        thread.start();
    }

    public void submit(AVPacket packet) {
        try {
            queue.put(packet);
        } catch (InterruptedException e) {
            Log.w(TAG, "Interrupted", e);
        }
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                AVPacket packet = queue.take();
                send(packet);
            }
        } catch (InterruptedException e) {
            // end of thread
        } catch (FFmpegException e) {
            Log.e(TAG, "Could not send packet", e);
        }
    }

    public void stop() {
        thread.interrupt();
    }

    public void release() {
        AVPacket packet;
        while ((packet = queue.poll()) != null) {
            packet.release();
        }
        releaseNative();
    }

    private static native void initIDs();
    private native boolean init(int width, int height, String dest);
    private native void releaseNative();
    private native void send(AVPacket packet) throws FFmpegException;
}
