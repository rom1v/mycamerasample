package com.rom1v.mycamerasample;

import android.view.Surface;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * A ref-counted surface.
 *
 * Always use {@code ref()} and {@code unref()} to hold or release a reference (do not called {@code surface.release()} manually).
 */
public final class SharedSurface {
    private final Surface surface;
    private final AtomicInteger arc;

    public SharedSurface(Surface surface) {
        this.surface = surface;
        arc = new AtomicInteger(1);
    }

    public Surface get() {
        return surface;
    }

    public void ref() {
        arc.incrementAndGet();
    }

    public boolean unref() {
        int refCount = arc.decrementAndGet();
        if (refCount < 0) {
            throw new IllegalStateException("unref() called for a surface already released");
        }
        if (refCount == 0) {
            surface.release();
            return true;
        }
        return false;
    }
}
