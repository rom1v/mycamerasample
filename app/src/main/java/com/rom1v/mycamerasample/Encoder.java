package com.rom1v.mycamerasample;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Encoder {

    private static final String TAG = MainActivity.TAG;
    private static final String MIMETYPE = MediaFormat.MIMETYPE_VIDEO_AVC;

    interface Callback {
        void onPacket(ByteBuffer codecBuffer, MediaCodec.BufferInfo bufferInfo);
    }

    private final Callback callback;

    private MediaCodec mediaCodec;
    private SharedSurface surface;

    private Thread thread;

    public Encoder(int width, int height, Callback callback) throws IOException {
        this.callback = callback;

        mediaCodec = MediaCodec.createEncoderByType(MIMETYPE);

        MediaFormat format = MediaFormat.createVideoFormat(MIMETYPE, width, height);
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT,MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE,8000000);
        format.setInteger(MediaFormat.KEY_FRAME_RATE,60);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,10);

        mediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        surface = new SharedSurface(mediaCodec.createInputSurface());
    }

    public SharedSurface getSurface() {
        return surface;
    }

    public synchronized void start() {
        if (thread != null) {
            throw new IllegalStateException("Encoder already started");
        }
        thread = new Thread(() -> {
            encode();
        });
        thread.start();
    }

    public synchronized void stop() {
        if (thread != null) {
            thread.interrupt();
        }
    }

    public synchronized void release() {
        if (thread == null) {
            // Only handle the case where destroy() is called but start() has not been called
            // Otherwise, the resources are released at the end of encode() (since it's asynchronous)
            mediaCodec.release();
            surface.unref();
        }
    }

    public void encode() {
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();

        mediaCodec.start();
        try {
            boolean eof = false;
            while (!eof && !Thread.currentThread().isInterrupted()) {
                int outputBufferId = mediaCodec.dequeueOutputBuffer(bufferInfo, -1);
                eof = (bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0;
                try {
                    if (outputBufferId >= 0) {
                        ByteBuffer codecBuffer = mediaCodec.getOutputBuffer(outputBufferId);
                        callback.onPacket(codecBuffer, bufferInfo);
                    }
                } finally {
                    if (outputBufferId >= 0) {
                        mediaCodec.releaseOutputBuffer(outputBufferId, false);
                    }
                }
            }
        } finally {
            mediaCodec.stop();
            mediaCodec.release();
            if (surface.unref()) {
                surface = null;
            }
        }
    }
}
