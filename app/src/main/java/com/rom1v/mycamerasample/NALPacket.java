package com.rom1v.mycamerasample;

public final class NALPacket {

    private int ssrc;
    private int ts;
    private byte[] data;

    private NALPacket(int ssrc, int ts, byte[] data) {
        this.ssrc = ssrc;
        this.ts = ts;
        this.data = data;
    }

    public static NALPacket createFrom(int ssrc, long timestampNs, byte[] payload, int offset, int length) {
        // Add 14 bytes in front to write RTP headers without reallocation/copy
        byte[] data = new byte[14 + length];
        System.arraycopy(payload, offset, data, 14, length);

        int ts = toRtpTimestamp(timestampNs);
        return new NALPacket(ssrc, ts, data);
    }

    public static int toRtpTimestamp(long timestampNs) {
        return (int) (timestampNs * 9 / 100000);
    }

    public static int findPrefix(byte[] buf, int offset) {
        int i = offset + 3;
        while (i < buf.length) {
            if (buf[i] == 1 && buf[i-1] == 0 && buf[i-2] == 0 && buf[i-3] == 0) {
                // found
                return i-3;
            }
            if (buf[i] == 0) {
                ++i;
            } else {
                i += 4;
            }
        }
        return -1;
    }

    public byte[] getData() {
        return data;
    }

    public int getOffset() {
        // hardcoded
        return 14;
    }

    public int getLength() {
        return data.length - 14;
    }

    public int getTs() {
        return ts;
    }

    public int getSsrc() {
        return ssrc;
    }
}
